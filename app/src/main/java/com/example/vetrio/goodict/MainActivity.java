package com.example.vetrio.goodict;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.vetrio.adapter.MainDictionaryPagerAdapter;
import com.example.vetrio.adapter.MainListViewNavigationAdapter;
import com.example.vetrio.customview.image.RoundImage;
import com.example.vetrio.parseservice.UserInfor;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.utils.ListViewHeightBasedOnChildren;
import com.example.vetrio.utils.Resources;
import com.example.vetrio.utils.RotateFab;


/**
 * Created by vetrio on 11/23/2015.
 */
public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Context context = this;
    private SearchView mSearchView;
    /**
     * Dictionary*/
    private View mDictionaryView;
    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private FloatingActionButton mFloatingActionButton;
    /*
    * Navigation*/
    private View mNavigationView;
    private TextView mUserName, mUserMail;
    private ImageView mUserPhoto;
    private ListView mListView1, mListView2;
    private boolean isFABOpening = false;
    private MainDictionaryPagerAdapter mMainDictionaryPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        * Find view by id to conect to mDictionaryView*/
        mDictionaryView = findViewById(R.id.activity_dictionary);
        mToolbar = (Toolbar) mDictionaryView.findViewById(R.id.dictionary_toolbar);
        mViewPager = (ViewPager) mDictionaryView.findViewById(R.id.dictionary_pager);
        mFloatingActionButton = (FloatingActionButton) mDictionaryView.findViewById(R.id.dictionary_fab_bt);

        /*
        * Find view by id to conect to mNavigationView*/
        mNavigationView = findViewById(R.id.main_navigation_view);
        mUserPhoto = (ImageView) mNavigationView.findViewById(R.id.user_photo);
        mUserName = (TextView) mNavigationView.findViewById(R.id.user_name);
        mUserMail = (TextView) mNavigationView.findViewById(R.id.user_email);
        mListView1 = (ListView) mNavigationView.findViewById(R.id.dict_list);
        mListView2 = (ListView) mNavigationView.findViewById(R.id.acount_list);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.main_drawer_layout);

        configToolBar();
        configFAB();
        configPageView();
        setUpNavigationView();
    }
    public void configFAB(){
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RecognizeActivity.class));
            }
        });
    }
    public void configPageView() {
        mMainDictionaryPagerAdapter = new MainDictionaryPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mMainDictionaryPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (isFABOpening) return;
                if (position != Constant.KANJI_TAB_POSITION) mFloatingActionButton.hide();
                else {
                    mFloatingActionButton.show();
                    RotateFab.rotateShow(mFloatingActionButton);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    public void setUpSearchView() {
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mMainDictionaryPagerAdapter.notifyNewQuery(query + "");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mMainDictionaryPagerAdapter.notifyNewQuery(newText + "");
                return false;
            }
        });
    }


    /*
    *Setting mToolbar*/
    public void configToolBar() {
        mToolbar.setTitle(R.string.app_name);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mDrawerToggle.setDrawerIndicatorEnabled(true);
    }

    /*
       * Setting Navigation*/
    public void setUpNavigationView() {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.admin);
        RoundImage roundedImage = new RoundImage(bm);
        //Change user photo
        mUserPhoto.setImageDrawable(roundedImage);
        mUserName.setText(UserInfor.USER_NAME);
        mUserMail.setText(UserInfor.USER_MAIL);
        //Set list
        mListView1.setAdapter(new MainListViewNavigationAdapter(context, Resources.menu1, Resources.icons_menu1));
        mListView2.setAdapter(new MainListViewNavigationAdapter(context, Resources.menu2, Resources.icons_menu2));
        ListViewHeightBasedOnChildren.setListViewHeightBasedOnChildren(mListView1);
        ListViewHeightBasedOnChildren.setListViewHeightBasedOnChildren(mListView2);
        mListView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*view.setSelected(true);*/
            }
        });
        mListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setSelected(true);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setQueryHint("Từ khóa");
        setUpSearchView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_kanji:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.action_vija:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.action_javi:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.action_hist:
                mViewPager.setCurrentItem(3);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        if (isFABOpening) {
            // mRevealLayout.revealMainView();
            isFABOpening = !isFABOpening;
        } else {
            super.onBackPressed();
        }
    }

}
