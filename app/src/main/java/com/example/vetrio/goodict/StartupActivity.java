package com.example.vetrio.goodict;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.library.loading.NewtonCradleLoading;
import com.example.vetrio.database.HistoryDataBaseHelper;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.database.DataBaseHeplper;
import com.example.vetrio.database.FavoriteDataBaseHelper;
import com.example.vetrio.database.UserDataBaseHelper;
import com.example.vetrio.parseservice.UserInfor;

import java.io.IOException;

public class StartupActivity extends AppCompatActivity {
    private NewtonCradleLoading newtonCradleLoading;
    private TextView loadingTextView;
    private String[] dataBaseNames = {Constant.GOODICT_DB_NAME, Constant.MINA_DB_NAME, Constant.JLPT_DB_NAME};
    private boolean isExits = true;
    private Context contex = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        loadingTextView = (TextView) findViewById(R.id.startup_loading_tv);
        //Loading library by victor
        newtonCradleLoading = (NewtonCradleLoading) findViewById(R.id.newton_cradle_loading);
        for (int i = 0; i < dataBaseNames.length; i++) {
            if(!checkDataBase(dataBaseNames[i])){
                isExits = false;
                break;
            }
        }
        if(!isExits){
            MyAsyncTask asyncTask = new MyAsyncTask();
            asyncTask.execute();
        } else {
            loggin();
        }
    }

    public boolean checkDataBase(String dbName) {
        DataBaseHeplper dataBaseHeplper = new DataBaseHeplper(this, dbName);
        return dataBaseHeplper.checkDataBase();
    }

    public void createDataBase(String dbName) {
        DataBaseHeplper dataBaseHeplper = new DataBaseHeplper(this, dbName);
        try {
            dataBaseHeplper.createDataBase();
            dataBaseHeplper.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class MyAsyncTask extends AsyncTask<Void, Integer, Void> {

        //hàm này sẽ được thực hiện đầu tiên
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            newtonCradleLoading.start();
            loadingTextView.setText("Đang tải...");
        }

        //sau đó tới hàm doInBackground
        //tuyệt đối không được cập nhật giao diện trong hàm này
        @Override
        protected Void doInBackground(Void... arg0) {

            for (int i = 0; i < dataBaseNames.length; i++) {
                createDataBase(dataBaseNames[i]);
                if(dataBaseNames[i] == Constant.GOODICT_DB_NAME){
                    new FavoriteDataBaseHelper(contex).createTable();
                    new HistoryDataBaseHelper(contex).createTable();
                }
                //khi gọi hàm này thì onProgressUpdate sẽ thực thi
                publishProgress();
            }
            return null;
        }
        /**
         * ta cập nhập giao diện trong hàm này
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        /**
         * sau khi tiến trình thực hiện xong thì hàm này sảy ra
         */
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            newtonCradleLoading.stop();
            loadingTextView.setText("Đã xong!");
            loggin();
        }

    }

    public boolean isEditUser(){
        UserDataBaseHelper mUserDataBaseHelper = new UserDataBaseHelper(this);
        Cursor cursor = mUserDataBaseHelper.getInformation();
        if(cursor == null || cursor.getCount() == 0) return false;
        cursor.moveToFirst();
        UserInfor.USER_NAME = cursor.getString(0);
        UserInfor.USER_MAIL = cursor.getString(1);
        return true;
    }

    public void loggin(){
        Intent intent;
        if(isEditUser()){
            intent = new Intent(StartupActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        } else {
            intent = new Intent(StartupActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
    }
}