package com.example.vetrio.goodict;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.library.materialtextfield.MaterialTextField;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.parseservice.ManageUsers;

public class LoginActivity extends AppCompatActivity {
    private EditText userEditText, passEditText, mailEditText;
    private TextView loginTextView, nextTextView, signupTextView;
    private MaterialTextField mailMaterialTextField;
    private LinearLayout animLayout, signupLayout;
    private int signupLayoutHeight; //Height of singupLayout
    private int log_sign = Constant.LOG_IN;
    private Context ctx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userEditText = (EditText) findViewById(R.id.login_user_ed);
        passEditText = (EditText) findViewById(R.id.login_pass_ed);
        mailEditText = (EditText) findViewById(R.id.login_mail_ed);
        loginTextView = (TextView) findViewById(R.id.login_login_bt);
        nextTextView = (TextView) findViewById(R.id.login_next_bt);
        signupTextView = (TextView) findViewById(R.id.login_signup_bt);
        signupLayout = (LinearLayout) findViewById(R.id.login_signup_layout);
        animLayout = (LinearLayout) findViewById(R.id.login_anim_layout);
        mailMaterialTextField = (MaterialTextField) findViewById(R.id.login_mail_mtf);


        nextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });

        signupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (log_sign) {
                    case Constant.LOG_IN:
                        goToSignUp();
                        break;
                    case Constant.SIGN_IN:
                        goToLogin();
                        break;
                    default:
                        System.out.println("Missing log_sign");
                        break;
                }
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (log_sign) {
                    case Constant.LOG_IN:
                        ManageUsers.login(ctx, userEditText.getText().toString(), passEditText.getText().toString());
                        break;
                    case Constant.SIGN_IN:
                        ManageUsers.signUp(ctx, userEditText.getText().toString(), passEditText.getText().toString(), mailEditText.getText().toString());
                        break;
                    default:
                        System.out.println("Missing log_sign");
                        break;
                }
            }
        });
    }

    public void goToSignUp(){
        moveDownAnimLayout();
        mailMaterialTextField.setVisibility(View.VISIBLE);
        log_sign = Constant.SIGN_IN;
        signupTextView.setText("Hủy bỏ");
        loginTextView.setText("Tiếp tục");
    }
    public void goToLogin(){
        moveUpAnimLayout();
        mailMaterialTextField.setVisibility(View.GONE);
        log_sign = Constant.LOG_IN;
        signupTextView.setText("Đăng ký");
        loginTextView.setText("Đăng nhập");
    }

    public void moveDownAnimLayout(){
        ObjectAnimator objectAnimator= ObjectAnimator.ofFloat(animLayout, "translationY", 0, signupLayoutHeight);
        objectAnimator.setDuration(500);
        objectAnimator.start();
        ObjectAnimator objectAnimator2= ObjectAnimator.ofFloat(signupLayout, "translationY", 0, signupLayoutHeight);
        objectAnimator2.setDuration(300);
        objectAnimator2.start();
    }
    public void moveUpAnimLayout(){
        ObjectAnimator objectAnimator= ObjectAnimator.ofFloat(animLayout, "translationY", signupLayoutHeight, 0);
        objectAnimator.setDuration(500);
        objectAnimator.start();
        ObjectAnimator objectAnimator2= ObjectAnimator.ofFloat(signupLayout, "translationY", signupLayoutHeight, 0);
        objectAnimator2.setDuration(600);
        objectAnimator2.start();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        signupLayoutHeight = signupLayout.getHeight();
    }
}
