package com.example.vetrio.goodict;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.library.fabreveallayout.FABRevealLayout;
import com.example.vetrio.adapter.RecogzineListViewAdapter;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.customview.canvasview.CanvasView;
import com.example.vetrio.converter.HtmlConverter;
import com.example.vetrio.database.DataBaseHeplper;
import com.example.vetrio.customview.fingeritem.FingerSearchItem;

import java.io.IOException;

public class RecognizeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public CanvasView drawView;
    public ImageButton undoButton, redoButton, clearButton, searchButton, backButton;
    public ProgressBar progressBar;
    private Dialog dialog;
    private ListView listView;
    private FABRevealLayout mRevealLayout;
    private FloatingActionButton mFloatingActionButton;

    private String dbName = Constant.GOODICT_DB_NAME;
    private String tableName = Constant.KANJI_TABLE_NAME;
    private String columnName = Constant.KANJI_COLUMN_NAME;

    public DataBaseHeplper dataBaseHeplper;
    public Cursor cursor;

    public Context context = this;

    private FingerSearchItem[] fingerSearchItems;
    private RecogzineListViewAdapter fingerArrayAdapter;

    private boolean isFABOpening = false;
    private boolean canBack = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognize);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawView = (CanvasView) findViewById(R.id.draw_view);
        undoButton = (ImageButton) findViewById(R.id.kanji_undo_bt);
        redoButton = (ImageButton) findViewById(R.id.kanji_redo_bt);
        clearButton = (ImageButton) findViewById(R.id.kanji_del_bt);
        searchButton = (ImageButton) findViewById(R.id.kanji_search_bt);
        backButton = (ImageButton) findViewById(R.id.dictionary_back_bt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRevealLayout = (FABRevealLayout) findViewById(R.id.kanji_fab_rl);
        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.kanji_fab_bt);

        progressBar.setVisibility(View.GONE);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(android.R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

        toolbar.setTitle("Tìm nét vẽ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, dbName);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        configFab();
        configRevealLayout();
    }

    public void configFab(){
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goSecondaryView();
            }
        });
    }

    public void configRevealLayout(){
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.clearCanvas();
            }
        });
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.undoCanvas();
            }
        });
        redoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.redoCanvas();
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(progressBar.isShown()){
                    Toast.makeText(RecognizeActivity.this, "Dữ liệu đang tải. Chờ một chút!", Toast.LENGTH_SHORT).show();
                    return;
                }
                canBack = false;
                try {
                    fingerSearchItems = drawView.updateFromChangeCanvas();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                getData();
                showDialog(fingerSearchItems);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMainView();
            }
        });
    }
    public void goMainView(){
        if(mFloatingActionButton.getVisibility() == View.GONE) mFloatingActionButton.setVisibility(View.INVISIBLE);
        mRevealLayout.revealMainView();
        //RotateFab.rotateBackWard(mFloatingActionButton);
        isFABOpening = !isFABOpening;
    }
    public void goSecondaryView(){
        mRevealLayout.revealSecondaryView();
        //RotateFab.rotateForward(mFloatingActionButton);
        isFABOpening = !isFABOpening;
    }

    private void showDialog(final FingerSearchItem[] fingerSearchItems) {

        dialog = new Dialog(this);
        listView = new ListView(context);
        fingerArrayAdapter = new RecogzineListViewAdapter(this, fingerSearchItems);
        listView.setAdapter(fingerArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (fingerSearchItems[position].getData() != null) {
                    String[] data = fingerSearchItems[position].getData();

                    Intent intent = new Intent(RecognizeActivity.this, ResultActivity.class);
                    intent.putExtra("dataHtml", new HtmlConverter().getHtmlKanji(data));
                    intent.putExtra("newWord", data[0]);
                    intent.putExtra("iskanji", true);
                    intent.putExtra("hanviet", data[1]);
                    startActivity(intent);
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.light_primary_background));
        dialog.setContentView(listView);
        dialog.setTitle("Kết quả tìm kiếm");
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (canBack) {
            super.onBackPressed();

            if(cursor != null) cursor.close();
            if(dataBaseHeplper != null) dataBaseHeplper.close();
            if(dialog != null && dialog.isShowing())
                dialog.dismiss();
        } else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }

    public void getData() {

        progressBar.setVisibility(View.VISIBLE);
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                for(int i = 0; i < fingerSearchItems.length; i++){
                    Cursor cursor = dataBaseHeplper.getInformation(tableName, columnName, fingerSearchItems[i].getKanji());
                    if(cursor.getCount() != 0)
                    {
                        cursor.moveToFirst();
                        String data[] = new String[7];

                        for (int j= 0; j < 7; j++){
                            data[j] = cursor.getString(j+1);
                        }
                        fingerSearchItems[i].setData(data);
                    }
                }
                return null;
            }

            protected void onProgressUpdate(Integer... values) {
            }
            protected void onPostExecute(Void result) {
                fingerArrayAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                canBack = true;
            }
        };
        asyncTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onStart(){
        super.onStart();
        goSecondaryView();
    }
}
