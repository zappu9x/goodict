package com.example.vetrio.goodict;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class ResultActivity extends AppCompatActivity {

    private WebView contentWebView;
    private Toolbar toolbar;
    private String dataHtml;
    private String newWord;
    private Boolean isKanjiQuery;
    private String hanViet;
    private TextView mKanjiTextView, mHanVietTextView;
    private TextToSpeech mTextToSpeech;
    private LinearLayout mLinearLayout;
    private Context ctx = this;
    private FloatingActionButton mFloatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mLinearLayout = (LinearLayout) findViewById(R.id.result_kanji_ll);
        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.result_fab_bt);
        getData();
        setUpTextToSpeech();
        setUpToolBar();
        setUpFAB();
        contentWebView = (WebView) findViewById(R.id.content_wv);
    }
    public void getData() {
        AsyncTask<Void, Integer, Void> simpleAsyncTask = new AsyncTask<Void, Integer, Void>() {
            Typeface font;
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Void doInBackground(Void... voids) {
                dataHtml = getIntent().getStringExtra("dataHtml");
                newWord = getIntent().getStringExtra("newWord");
                isKanjiQuery = getIntent().getBooleanExtra("iskanji", false);
                if (isKanjiQuery) {
                    font = Typeface.createFromAsset(getAssets(), "fonts/KanjiStrokeOrders_v3.001.ttf");
                }
                publishProgress();
                return null;
            }

            protected void onProgressUpdate(Integer... values) {
                contentWebView.loadDataWithBaseURL(null,dataHtml, "text/html", "utf-8",null);
                if (!isKanjiQuery){
                    mLinearLayout.setVisibility(View.GONE);
                }
                else {
                    mKanjiTextView = (TextView) findViewById(R.id.result_kanji);
                    mHanVietTextView = (TextView) findViewById(R.id.result_hanviet);
                    hanViet = getIntent().getStringExtra("hanviet");
                    mKanjiTextView.setTypeface(font);
                    mKanjiTextView.setText(newWord);
                    mHanVietTextView.setText(hanViet);
                }
            }
            protected void onPostExecute(Void result) {
            }
        };
        simpleAsyncTask.execute();
    }

    public void setUpTextToSpeech(){
        mTextToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTextToSpeech.setLanguage(Locale.JAPANESE);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                        Toast.makeText(ctx, "This text to speech not supported", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void setUpToolBar(){
        toolbar.setTitle("Kết quả tra từ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public void setUpFAB(){
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openHeading();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openHeading(){
        mTextToSpeech.speak(newWord, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void onPause(){
        if(mTextToSpeech !=null){
            mTextToSpeech.stop();
        }
        super.onPause();
    }

    public void onDestroy(){
        if(mTextToSpeech !=null){
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();
        }
        super.onDestroy();
    }
}
