package com.example.vetrio.utils;

import android.animation.ObjectAnimator;
import android.support.design.widget.FloatingActionButton;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;

/**
 * Created by nguye on 12/17/2015.
 */
public class RotateFab {
    public static void rotateForward(FloatingActionButton mFloatingActionButton){
        ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(mFloatingActionButton ,
                "rotation", 0f, 45f);
        imageViewObjectAnimator.setDuration(300); // miliseconds
        imageViewObjectAnimator.start();
    }
    public static void rotateBackWard(FloatingActionButton mFloatingActionButton){
        ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(mFloatingActionButton ,
                "rotation", 45f, 0f);
        imageViewObjectAnimator.setDuration(300); // miliseconds
        imageViewObjectAnimator.start();
    }

    public static void rotateShow(FloatingActionButton mFloatingActionButton){
        // Rotate Animation
        Animation rotate = new RotateAnimation(60.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(150);
        rotate.setInterpolator(new DecelerateInterpolator());
        mFloatingActionButton.startAnimation(rotate);
    }
}
