package com.example.vetrio.utils;

import com.example.vetrio.goodict.R;

/**
 * Created by nguye on 11/25/2015.
 */
public class Resources {
    public static String[] menu1 = {
            "Từ mới của ngày",
            "Danh sách yêu thích",
            "Kiểm tra thú vị",
            "Kiến thức mina",
            "Cài đặt"
    };
    public static String[] menu2 = {
            "Tài khoản của tôi",
            "Đồng bộ tài khoản",
            "Trợ giúp và phản hồi"
    };

    public static int[] icons_menu1 = {
            R.drawable.ic_bookmark_grey_24dp,
            R.drawable.ic_star_grey_24dp,
            R.drawable.ic_file_document_box_grey_24dp,
            R.drawable.ic_book_grey_24dp,
            R.drawable.ic_settings_grey_24dp
    };
    public static int[] icons_menu2 = {
            R.drawable.ic_account_box_grey_24dp,
            R.drawable.ic_sync_grey_24dp,
            R.drawable.ic_help_grey_24dp

    };
}
