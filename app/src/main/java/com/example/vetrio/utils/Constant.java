package com.example.vetrio.utils;

/**
 * Created by nguye on 11/30/2015.
 */
public interface Constant {
    /*LoginActivity*/
    public static final int LOG_IN = 0;
    public static final int SIGN_IN = 1;

    /*path save data*/
    public static final String DATABASE_PATH = "/data/data/com.example.vetrio.goodict/databases/";

    /*Goodict database*/
    public static final String GOODICT_DB_NAME = "Goodict.db";
    public static final int GOODICT_DATA_VALUE = 34189;

    public static final String JAVI_TABLE_NAME = "japanese_vietnamese";
    public static final String JAVI_QUERY_COLUMN_NAME = "word";
    public static final int JAVI_TAB_POSITION = 1;

    public static final String VIJA_TABLE_NAME = "vietnamese_japanese";
    public static final String VIJA_QUERY_COLUMN_NAME = "word";
    public static final int VIJA_TAB_POSITION = 2;

    public static final String KANJI_TABLE_NAME = "kanjidata";
    public static final String KANJI_HANVIET_COLUMN_NAME = "hanviet";
    public static final String KANJI_COLUMN_NAME = "kanji";
    public static final int KANJI_TAB_POSITION = 0;

    public static final String HISTORY_TABLE_NAME = "history";
    public static final String[] HISTORY_COLUMN_NAME = {"_id", "word", "mean", "date", "type"};
    public static final int HISTORY_KANJI_TYPE = 0;
    public static final int HISTORY_JAVI_TYPE = 1;
    public static final int HISTORY_VIJA_TYPE = 2;
    public static final int HISTORY_TAB_POSITION = 3;

    public static final String FV_JAVI_TABLE_NAME = "fav_ja_vi";
    public static final String FV_VIJA_TABLE_NAME = "fav_vi_ja";
    public static final String FV_KANJI_TABLE_NAME = "fav_kanji";
    public static final String FV_COLUMN_NAME = "idposition";

    public static final String USER_TABLE_NAME = "currentuser";
    public static final String USER_NAME = "username";
    public static final String USER_MAIL = "email";

    /*JLPT database*/
    public static final String JLPT_DB_NAME = "JLPTTest.db";
    public static final int JLPT_DATA_VALUE = 533;

    /*Minabook database*/
    public static final String MINA_DB_NAME = "Minabook.db";
    public static final int MINA_DATA_VALUE = 832;

    public static final String JAVI_TAB_NAME = "Nhật Việt";
    public static final String VIJA_TAB_NAME = "Việt Nhật";
    public static final String KANJI_TAB_NAME = "Hán Tự";
    public static final String HISTORY_TAB_NAME = "Lịch Sử";

}
