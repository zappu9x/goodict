package com.example.vetrio.converter;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by nguye on 9/22/2015.
 */
public class HtmlConverter {
    public String getMean(String content) {
        String string = "";
        char a[] = content.toCharArray();
        boolean isStart = false;
        boolean isContinue = false;
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '◆') {
                isStart = true;
            }
            if (a[i] == '※' || a[i] == '-' || a[i] == '∴' || a[i] == '☆') {
                isStart = false;
            }
            if (isStart == true && a[i] != '◆') {
                string += a[i];
                isContinue = true;
            }
            if (isContinue == true && a[i] == '◆') {
                string += '/';
                isContinue = true;
            }
        }
        return string;
    }


    public String getMeanKanji(String content) {
        String string = "";
        char a[] = content.toCharArray();
        boolean isStart = false;
        boolean isContinue = false;
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '：') {
                isStart = true;
            }
            if (a[i] == ';') {
                isStart = false;
            }
            if (isStart == true && a[i] != '：') {
                string += a[i];
                isContinue = true;
            }
            if (isContinue == true && a[i] == ';') {
                string += '/';
                isContinue = false;
            }
        }
        return string;
    }

    public String getKunyomiKanji(String content){
        String string = "";
        ArrayList<String> list = new ArrayList<String>();
        char a[] = content.toCharArray();
        boolean isOpen = true;
        for (int i = 0; i < content.length(); i++) {
            if(!isOpen){
                if (a[i] == ';') {
                    isOpen = true;
                    continue;
                }
                if (a[i] == '、') isOpen = true;
            }

            if (isOpen) {
                if(a[i] < 10000){
                    isOpen = false;
                    if(a[i] != '.' && a[i] != '：'&& a[i] != '、') continue;
                }
                if (a[i] == '.' || a[i] == '：'|| a[i] == '、'){
                    isOpen = false; //close
                    if(!string.equals("")) {
                        list.add(string);
                        string = "";
                        Log.d("html", string);
                    }
                }
                else
                    string += a[i];
            }
        }


        Log.d("html", list.size()+"");

        String result = "";
        if(list.isEmpty()) return "";
        if(list.size() == 1) return list.get(0);

        for (int i = 0; i < list.size(); i++){
            for (int j = i+1; j < list.size() - 1; j++){
                if(list.get(j).equals(list.get(i))) list.remove(j);
            }
        }

        Log.d("html", list.size()+"aaaa");
        for (int i = 0; i < list.size(); i++){
            result += list.get(i);
            if(i < list.size()-1) result += '、';
        }
        return result;
    }

    public String getHtml(String word, String content) {
        String string = "";
        string += "<H3><Font color = \"#dc0e00\">" + word + "</Font></H3><ul>";
        char a[] = content.toCharArray();
        String openCard = "";
        String closeCard = "";
        for (int i = 0; i < content.length(); i++) {
            if (a[i] == '∴') {
                string += closeCard;
                openCard = "</ul><hr><br/><Font color = \"#dc0e00\" style=\"font-size: 18px;\" >";
                closeCard = "</Font>";
                string += openCard;
            }
            if (a[i] == '☆') {
                string += closeCard;
                openCard = "</ul><br/><Font color = \"#8706B0\">";
                closeCard = "</Font><ul>";
                string += openCard;
            }
            if (a[i] == '◆') {
                string += closeCard;
                openCard = "</ul><li><Font color = \"#000000\" style=\"font-size: 16px;\" >";
                closeCard = "</Font></b><ul>";
                string += openCard;
            }

            if (a[i] == '※') {
                string += closeCard;
                openCard = "<li><Font color = \"#262626\" style=\"font-size: 14px;\" > <a target=\"_blank\" href=\"entry://example\"><Font color = \"#262626\" >";
                closeCard = "</a>";
                string += openCard;
            }

            if (a[i] == ':') {
                string += closeCard;
                closeCard = "</Font></li>";
            }
            if (a[i] != '※' && a[i] != '◆' && a[i] != '∴')
                string += a[i];
        }
        string += "</ul>";
        Log.d("html", string);
        return string;
    }

    //String kanji, String hanviet, String radical, String strocke, String onread, String kunread, String example
    public String getHtmlKanji(String[] content) {
        String string = "";
        string += "<li><Font style=\"font-size: 16px;\">Bộ. </Font><Font color = \"#262626\" style=\"font-size: 14px;\">" + content[2] + "</Font></li>"
                + "<li><Font style=\"font-size: 16px;\">Số nét. </Font><Font color = \"#262626\" style=\"font-size: 14px;\">" + content[3] + "</Font></li>"
                + "<li><Font style=\"font-size: 16px;\">Onyomi. </Font></li><ul><li><Font color = \"#262626\" style=\"font-size: 14px;\">" + content[4] + "</Font></li></ul>"
                + "<li><Font style=\"font-size: 16px;\">Kunyomi. </Font></li><Font color = \"#262626\" style=\"font-size: 14px;\">"
                + "<ul><li>";
        char a[] = content[5].toCharArray();
        String openCard = "<li>";
        String closeCard = "</li>";
        for (int i = 0; i < content[5].length(); i++) {
            if (a[i] == ';') {
                string += closeCard + openCard;
            }
            if (a[i] != ';') {
                string += a[i];
            }
        }

        // Example to html
        string += "</li></ul></font>"
                + "<li><Font style=\"font-size: 16px;\">Ví dụ. </Font></li><Font color = \"#262626\" style=\"font-size: 14px;\"><ul>";

        if (content[6] != null) {
            a = content[6].toCharArray();
            openCard = "<li><a target=\"_blank\" href=\"entry://example\"><Font style=\"color:red; font-size: 14px;\">";
            closeCard = "</li>";
            int numberOfDot = 0;
            int numberBefore = 48;
            for (int i = 0; i < content[6].length(); i++) {
                if (a[i] > 48 && a[i] <= 57){
                    if((a[i] - numberBefore == 1) && ((i+1) <= content[6].length()) && (a[i+1] != 32)) {
                        /***
                         * do nothing*/
                        Log.d("htmlffff", a[i+1]+"");
                    }
                    else {
                        numberOfDot = 0;
                        if (a[i] - numberBefore == 1) {
                            string += closeCard + openCard;
                            numberBefore = a[i];
                            continue;
                        }
                    }
                }
                if (a[i] == ':') {
                    numberOfDot++;
                    switch (numberOfDot) {
                        case 1:
                            string += "</Font></a><Font style=\"color:red; font-size: 14px;\"> : ";
                            break;
                        case 2:
                            string += "</Font>.<br>";
                            break;
                        case 3:
                            string += "<br>";
                            break;
                        default:
                    }
                    continue;
                }
                string += a[i];
            }
        }
        string += "</li></Font></ul>";

        Log.d("html", string);
        Log.d("html122345", a.length + "");
        return string;
    }
}