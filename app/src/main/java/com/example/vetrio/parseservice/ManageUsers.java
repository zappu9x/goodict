package com.example.vetrio.parseservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.vetrio.database.UserDataBaseHelper;
import com.example.vetrio.goodict.MainActivity;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by nguye on 12/21/2015.
 */
public class ManageUsers {
    public static void signUp(final Context ctx, String username, String password, String email){

        // Validate the sign up data
        boolean validationError = false;
        String error = "Không được để trống ";
        if (username.length() == 0) {
            validationError = true;
            error += "user";

        }
        if (password.length() == 0) {
            if (validationError) {
                error += ", ";
            }
            validationError = true;
            error += "pass";
        }
        if (email.length() == 0) {
            if (validationError) {
                error += ", ";
            }
            validationError = true;
            error += "email";
        }

        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(ctx, error, Toast.LENGTH_LONG).show();
            return;
        }

        // Set up a progress dialog
        final ProgressDialog dialog = new ProgressDialog(ctx);
        dialog.setTitle("Đăng ký");
        dialog.setMessage("Xin đợi một lát.");
        dialog.show();
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                // Handle the response
                dialog.dismiss();
                if (e == null) {
                    onLoginSuccess(ctx);
                } else {
                    Toast.makeText(ctx, "Đăng ký không thành công! Vui lòng thử lại.\nMã lỗi: \"" + e.getMessage() + "\"", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public static void login(final Context ctx, final String username, final String password) {

        // Validate the sign up data
        boolean validationError = false;
        String error = "Không được để trống ";
        if (username.length() == 0) {
            validationError = true;
            error += "user";

        }
        if (password.length() == 0) {
            if (validationError) {
                error += ", ";
            }
            validationError = true;
            error += "pass";
        }
        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(ctx, error, Toast.LENGTH_LONG).show();
        }

        // Set up a progress dialog
        final ProgressDialog dialog = new ProgressDialog(ctx);
        dialog.setTitle("Đăng nhập");
        dialog.setMessage("Xin đợi một lát.");
        dialog.show();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                dialog.dismiss();
                if (e == null) {
                    onLoginSuccess(ctx);
                } else {
                    Toast.makeText(ctx, "Đăng nhập không thành công! Vui lòng thử lại.\nMã lỗi: \"" + e.getMessage() + "\"", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public static void onLoginSuccess(Context ctx){
        Toast.makeText(ctx, "Đăng nhập thành công!", Toast.LENGTH_LONG).show();
        ParseUser currentUser = ParseUser.getCurrentUser();
        UserInfor.USER_NAME = currentUser.getUsername();
        UserInfor.USER_MAIL = currentUser.getEmail();
        new UserDataBaseHelper(ctx).insertInformations(currentUser.getUsername(), currentUser.getEmail());
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(intent);
    }
}
