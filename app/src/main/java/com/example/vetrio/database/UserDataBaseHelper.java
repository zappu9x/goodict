package com.example.vetrio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vetrio.utils.Constant;

public class UserDataBaseHelper extends SQLiteOpenHelper {
	private String dbPath = Constant.DATABASE_PATH;
	private final Context context;
	public static String userDbName = Constant.GOODICT_DB_NAME;
	private String userColumnName = Constant.USER_NAME;
	private String mailColumnName = Constant.USER_MAIL;
	private String userTableName = Constant.USER_TABLE_NAME;

	public UserDataBaseHelper(Context context) {
		super(context, userDbName, null, 1);
		this.context = context;
	}

	public boolean checkDataBase(String dbName) {

		SQLiteDatabase checkDB = null;
		try {
			checkDB = openDataBase(dbName);
		} catch (SQLiteException e) {
			//database does't exist yet.
		}
		if (checkDB != null) {
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}

	public SQLiteDatabase openDataBase(String dbName) throws SQLException {
		String myPath = dbPath + dbName;
		SQLiteDatabase sqLiteDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		sqLiteDatabase.close();
		return sqLiteDatabase;
	}

	public void insertInformations(String userName, String email){
		SQLiteDatabase SQL = this.getWritableDatabase();
		SQL.execSQL("delete from " + userTableName);
		ContentValues insertValues = new ContentValues();
		insertValues.put(userColumnName, userName);
		insertValues.put(mailColumnName, email);
		SQL.insert(userTableName, null, insertValues);
	}

	public Cursor getInformation(){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ userTableName, null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public void deleteInfomation(String username){
		SQLiteDatabase SQL = this.getWritableDatabase();
		SQL.delete(userTableName, userColumnName + "=?", new String[]{username+""});
	}


	@Override
	public void onCreate(SQLiteDatabase sdb) {
		boolean dbExist = checkDataBase(userDbName);
		if (dbExist) {
			//do nothing - database already exist
		} else {
			String CreatQuery = "CREATE TABLE " + userTableName +
					"( " 	+ userColumnName + " TEXT, "
							+ mailColumnName + " TEXT, " +
					"Primary key ("
							+ userColumnName + ","
							+ mailColumnName + ")" +
					");";
			sdb.execSQL(CreatQuery);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
