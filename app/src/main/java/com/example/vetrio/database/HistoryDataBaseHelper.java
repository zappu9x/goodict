package com.example.vetrio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vetrio.converter.KanaConverter;
import com.example.vetrio.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryDataBaseHelper extends SQLiteOpenHelper {
	private Context context;
	public static String dataName = Constant.GOODICT_DB_NAME;
	private String[] columnName = Constant.HISTORY_COLUMN_NAME;
	private String tableName = Constant.HISTORY_TABLE_NAME;

	public HistoryDataBaseHelper(Context context) {
		super(context, dataName, null, 1);
		this.context = context;
	}

	public void insertInformations(int index, String word, String mean, int type){
		deleteInfomation(index, type);
		SQLiteDatabase SQL = this.getWritableDatabase();
		ContentValues insertValues = new ContentValues();
		insertValues.put(columnName[0], index);
		insertValues.put(columnName[1], word);
		insertValues.put(columnName[2], mean);
		insertValues.put(columnName[3], new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
		insertValues.put(columnName[4], type);
		SQL.insert(tableName, null, insertValues);
	}
	public Cursor getInformationByIndex(int index){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName + " where " + columnName[0] + "=" + index +
					" order by "+ columnName[3] + "  DESC;", null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public Cursor getInformationByDate(){
		String date = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName + " where " + columnName[3] + "= \"" + date +
					"\" order by "+ columnName[3] + "  DESC;", null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public Cursor getInformationByDate(String column, String wordQuery){
		String date = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try {
			result = SQL.rawQuery("Select * from " + tableName + " where ( " +
					column + " like \"" + wordQuery + "%\" or " +
					column + " like \"" + KanaConverter.toHiragana(wordQuery) + "%\") and " + columnName[3] + "= \"" + date +
					"\" order by "+ columnName[3] + "  DESC;", null);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	public Cursor getInformationBeforeDate(String date){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName + " where " + columnName[3] + " not in " +
					"( Select " + columnName[3] + "  from "+ tableName + " where " + columnName[3] + "= \"" + date + "\" )" +
					" order by "+ columnName[3] + "  DESC;", null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public Cursor getInformationBeforeDate(String column, String wordQuery) {
		String date = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try {
			result = SQL.rawQuery("Select * from " + tableName + " where ( " +
					column + " like \"" + wordQuery + "%\" or " +
					column + " like \"" + KanaConverter.toHiragana(wordQuery) + "%\" ) and " + columnName[3] + " not in " +
					"( Select " + columnName[3] + "  from "+ tableName + " where " + columnName[3] + "=\"" + date + "\" )" +
					" order by "+ columnName[3] + "  DESC;", null);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	public Cursor getInformation(String tableName){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName, null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public Cursor getInformation(String column, String wordQuery) {
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try {
			result = SQL.rawQuery("Select * from " + tableName + " where " +
					column + " like \"" + wordQuery + "%\" or " +
					column + " like \"" + KanaConverter.toHiragana(wordQuery) + "%\"" +
					"order by "+ columnName[3] + "  DESC;", null);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	public void deleteInfomation(int index, int type){
		SQLiteDatabase SQL = this.getWritableDatabase();
		SQL.delete(tableName, columnName[0] + "=" + index + " and " + columnName[4] + "=" + type, null);
	}

	public void createTable(){
		SQLiteDatabase sdb = getWritableDatabase();
		String query = "CREATE TABLE IF NOT EXISTS " + tableName + "( " +
				columnName[0] + " INTEGER , "+ 				//id
				columnName[1] + " TEXT , "+					//word
				columnName[2] + " TEXT , "+					//mean
				columnName[3] + " TEXT , "+					//date
				columnName[4] + " INTEGER," +				//type
				"PRIMARY KEY ( " + columnName[0] + "," + columnName[4] +"));";
		sdb.execSQL(query);
	}

	@Override
	public void onCreate(SQLiteDatabase sdb) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
