package com.example.vetrio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.vetrio.utils.Constant;

public class FavoriteDataBaseHelper extends SQLiteOpenHelper {
	private final Context context;
	public static String dataName = Constant.GOODICT_DB_NAME;
	private String columnName = Constant.FV_COLUMN_NAME;
	private String javiTableName = Constant.FV_JAVI_TABLE_NAME;
	private String vijaTableName = Constant.FV_VIJA_TABLE_NAME;
	private String kanjiTableName = Constant.FV_KANJI_TABLE_NAME;

	public FavoriteDataBaseHelper(Context context) {
		super(context, dataName, null, 1);
		this.context = context;
	}

	public void insertInformations(String tableName, int index){
		Log.d("00000", index + "index\n" + tableName + "table");
		SQLiteDatabase SQL = this.getWritableDatabase();
		ContentValues insertValues = new ContentValues();
		insertValues.put(columnName, index);
		SQL.insert(tableName, null, insertValues);
	}
	public Cursor getInformation(String tableName, int index){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName + " where " + columnName + "=" + index, null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public Cursor getInformation(String tableName){
		SQLiteDatabase SQL = this.getReadableDatabase();
		Cursor result;
		try{
			result = SQL.rawQuery("Select * from "+ tableName, null);
		}
		catch(Exception e){
			result = null;
		}
		return result;
	}

	public void deleteInfomation(String tableName, int index){
		SQLiteDatabase SQL = this.getWritableDatabase();
		SQL.delete(tableName, columnName + "=?", new String[]{index+""});
	}

	public void createTable(){
		SQLiteDatabase sdb = getWritableDatabase();
		sdb.execSQL(queryCreateTableByName(javiTableName));
		sdb.execSQL(queryCreateTableByName(vijaTableName));
		sdb.execSQL(queryCreateTableByName(kanjiTableName));
	}

	public String queryCreateTableByName(String tableName){
		return "CREATE TABLE IF NOT EXISTS " + tableName + "( " + columnName + " INTEGER PRIMARY KEY);";
	}

	@Override
	public void onCreate(SQLiteDatabase sdb) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
