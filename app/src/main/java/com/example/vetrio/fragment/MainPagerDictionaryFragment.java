package com.example.vetrio.fragment;

import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vetrio.adapter.MainRecyclerJaViAdapter;
import com.example.vetrio.adapter.MainRecyclerKanjiAdapter;
import com.example.vetrio.adapter.MainRecyclerViJaAdapter;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.converter.KanaConverter;
import com.example.vetrio.customview.recyclerview.DividerItemDecoration;
import com.example.vetrio.database.DataBaseHeplper;
import com.example.vetrio.goodict.R;

/**
 * Created by nguye on 12/10/2015.
 */

public class MainPagerDictionaryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String dataBaseNames = Constant.GOODICT_DB_NAME;
    private String[] tableNames = {
                    Constant.KANJI_TABLE_NAME,
                    Constant.JAVI_TABLE_NAME,
                    Constant.VIJA_TABLE_NAME };
    private String[] columnNames = {
                    Constant.KANJI_HANVIET_COLUMN_NAME,
                    Constant.JAVI_QUERY_COLUMN_NAME,
                    Constant.VIJA_QUERY_COLUMN_NAME };

    private String textSubmit = "";
    private String textQuery = "";
    private Cursor cursor, newCursor;
    private DataBaseHeplper dataBaseHeplper;
    private int position;

    public static boolean canBack = false;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        dataBaseHeplper = new DataBaseHeplper(getActivity().getBaseContext(), dataBaseNames);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        KanaConverter.init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_viewpager_dictionary, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.dicitonary_srl);
        recyclerView = (RecyclerView) view.findViewById(R.id.dictionary_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.primary);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                queryDatabase();
            }
        });
        return view;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        queryDatabase();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        newCursor.close();
        dataBaseHeplper.close();
    }

    public void setNewQueryText(String queryText) {
        textSubmit = queryText;
        queryDatabase();
    }

    private void queryDatabase() {
        swipeRefreshLayout.setRefreshing(true);
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                textQuery = textSubmit;
                if(position == Constant.JAVI_TAB_POSITION) {
                    try {
                        textQuery = KanaConverter.toHiragana(textQuery);
                    } catch (Exception e) {
                    /*Toast.makeText(getActivity().getBaseContext(), "Sai từ khóa", Toast.LENGTH_SHORT).show();*/
                    }
                }
                newCursor = dataBaseHeplper.getInformation(tableNames[position], columnNames[position], textQuery);
                newCursor.moveToFirst();
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                canBack = true;
                if (newCursor.getCount() == 0) {
                    /*Toast.makeText(getActivity().getBaseContext(), "Sai từ khóa", Toast.LENGTH_SHORT).show();*/
                }
                else {
                    cursor = newCursor;
                    switch (position){
                        case Constant.JAVI_TAB_POSITION :
                            MainRecyclerJaViAdapter mainRecyclerJaViAdapter = new MainRecyclerJaViAdapter(cursor, getContext());
                            recyclerView.setAdapter(mainRecyclerJaViAdapter);
                            break;
                        case Constant.VIJA_TAB_POSITION :
                            MainRecyclerViJaAdapter mainRecyclerViJaAdapter = new MainRecyclerViJaAdapter(cursor, getContext());
                            recyclerView.setAdapter(mainRecyclerViJaAdapter);
                            break;
                        case Constant.KANJI_TAB_POSITION :
                            MainRecyclerKanjiAdapter mainRecyclerKanjiAdapter = new MainRecyclerKanjiAdapter(cursor, getContext());
                            recyclerView.setAdapter(mainRecyclerKanjiAdapter);
                            break;
                        default:
                    }

                }
                swipeRefreshLayout.setRefreshing(false);
            }
        };
        asyncTask.execute();
    }
}