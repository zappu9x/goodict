package com.example.vetrio.fragment;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.vetrio.adapter.MainListViewHistoryAdapter;
import com.example.vetrio.database.HistoryDataBaseHelper;
import com.example.vetrio.goodict.R;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.utils.ListViewHeightBasedOnChildren;

/**
 * Created by nguye on 12/10/2015.
 */

public class MainPagerHistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String[] columnNames = Constant.HISTORY_COLUMN_NAME;
    private String textSubmit = "";
    private String textQuery = "";
    private Cursor todayCursor, oldCursor;
    private HistoryDataBaseHelper dataBaseHeplper;

    public static boolean canBack = false;

    private ListView todayListView, oldListView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataBaseHeplper = new HistoryDataBaseHelper(getActivity().getBaseContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_viewpager_history, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.dicitonary_srl);
        todayListView = (ListView) view.findViewById(R.id.dictionary_history_today_list);
        oldListView = (ListView) view.findViewById(R.id.dictionary_history_more_list);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.primary);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                queryDatabase();
            }
        });
        return view;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        queryDatabase();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(todayCursor != null) todayCursor.close();
        if(oldCursor != null) oldCursor.close();
        if(dataBaseHeplper != null) dataBaseHeplper.close();
    }

    public void setNewQueryText(String queryText) {
        textSubmit = queryText;
        queryDatabase();
    }

    private void queryDatabase() {
        swipeRefreshLayout.setRefreshing(true);
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                textQuery = textSubmit;
                todayCursor = dataBaseHeplper.getInformationByDate(columnNames[1], textQuery);
                if(todayCursor != null) todayCursor.moveToFirst();
                oldCursor = dataBaseHeplper.getInformationBeforeDate(columnNames[1], textQuery);
                if(oldCursor != null) oldCursor.moveToFirst();
                return null;
            }

            protected void onPostExecute(Void result) {
                canBack = true;
                todayListView.setAdapter(new MainListViewHistoryAdapter(getContext(), todayCursor));
                ListViewHeightBasedOnChildren.setListViewHeightBasedOnChildren(todayListView);

                oldListView.setAdapter(new MainListViewHistoryAdapter(getContext(), oldCursor));
                ListViewHeightBasedOnChildren.setListViewHeightBasedOnChildren(oldListView);

                swipeRefreshLayout.setRefreshing(false);
            }
        };
        asyncTask.execute();
    }
}