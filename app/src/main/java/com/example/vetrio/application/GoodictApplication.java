package com.example.vetrio.application;

import android.app.Application;

import com.example.vetrio.parseservice.ParseConstant;
import com.parse.Parse;
import com.parse.ParseInstallation;

public class GoodictApplication extends Application {
@Override
public void onCreate() {
    super.onCreate();
    Parse.initialize(this, ParseConstant.PARSE_APP_ID, ParseConstant.PARSE_APP_KEY);
    ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}