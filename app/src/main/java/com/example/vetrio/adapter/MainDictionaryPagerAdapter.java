package com.example.vetrio.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.vetrio.fragment.MainPagerHistoryFragment;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.fragment.MainPagerDictionaryFragment;

public class MainDictionaryPagerAdapter extends FragmentPagerAdapter{
    private MainPagerDictionaryFragment mJaviFragment;
    private MainPagerDictionaryFragment mVijaFragment;
    private MainPagerDictionaryFragment mKanjiFragment;
    private MainPagerHistoryFragment mHistoryFragment;

    private String[] mTitles = {Constant.JAVI_TAB_NAME, Constant.VIJA_TAB_NAME, Constant.KANJI_TAB_NAME, Constant.HISTORY_TAB_NAME};

    public MainDictionaryPagerAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        switch (position) {
            case Constant.JAVI_TAB_POSITION:
                mJaviFragment = new MainPagerDictionaryFragment();
                bundle.putInt("position", position);
                mJaviFragment.setArguments(bundle);
                return mJaviFragment;
            case Constant.VIJA_TAB_POSITION:
                mVijaFragment = new MainPagerDictionaryFragment();
                bundle.putInt("position", position);
                mVijaFragment.setArguments(bundle);
                return mVijaFragment;
            case Constant.KANJI_TAB_POSITION:
                mKanjiFragment = new MainPagerDictionaryFragment();
                bundle.putInt("position", position);
                mKanjiFragment.setArguments(bundle);
                return mKanjiFragment;
            case Constant.HISTORY_TAB_POSITION:
                mHistoryFragment = new MainPagerHistoryFragment();
                return mHistoryFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 4;
    }

    public void notifyNewQuery(String query) {
        if (query == null) {
            return;
        }
        if (mJaviFragment != null) {
            mJaviFragment.setNewQueryText(query);
        }
        if (mVijaFragment != null) {
            mVijaFragment.setNewQueryText(query);
        }
        if (mKanjiFragment != null) {
            mKanjiFragment.setNewQueryText(query);
        }
        if (mHistoryFragment != null) {
            mHistoryFragment.setNewQueryText(query);
        }
    }

    @Override
    public CharSequence getPageTitle(int position){
        return mTitles[position];
    }
}
