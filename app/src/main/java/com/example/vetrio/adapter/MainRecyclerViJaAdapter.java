package com.example.vetrio.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vetrio.database.HistoryDataBaseHelper;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.converter.HtmlConverter;
import com.example.vetrio.database.FavoriteDataBaseHelper;
import com.example.vetrio.goodict.R;
import com.example.vetrio.goodict.ResultActivity;

public class MainRecyclerViJaAdapter extends RecyclerView.Adapter<MainRecyclerViJaAdapter.ViewHolder> {

    private Cursor cursor;
    private FavoriteDataBaseHelper mFavoriteDataBaseHelper;
    private Context context;

    public MainRecyclerViJaAdapter(Cursor cursor, Context context) {
        this.cursor = cursor;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_dictionary_jv, parent, false);
        ViewHolder viewHolder = new ViewHolder(cursor, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.primary_tv.setText(cursor.getString(1));
        holder.secondary_tv.setText(new HtmlConverter().getMean(cursor.getString(2)));
        if(isFavorite(position)) holder.favorite_iv.setImageResource(R.drawable.ic_star_20dp);
        else holder.favorite_iv.setImageResource(R.drawable.ic_star_outline_20dp);

        holder.favorite_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFavorite(position)) {
                    mFavoriteDataBaseHelper.insertInformations(Constant.FV_VIJA_TABLE_NAME, position);
                    holder.favorite_iv.setImageResource(R.drawable.ic_star_20dp);
                    Toast.makeText(context, "Đã thêm "+ holder.primary_tv.getText()+ " vào danh sách yêu thích", Toast.LENGTH_SHORT).show();
                }
                else{
                    mFavoriteDataBaseHelper.deleteInfomation(Constant.FV_VIJA_TABLE_NAME, position);
                    holder.favorite_iv.setImageResource(R.drawable.ic_star_outline_20dp);
                    Toast.makeText(context, "Đã hủy "+ holder.primary_tv.getText()+ " từ danh sách yêu thích", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public boolean isFavorite(int position){
        mFavoriteDataBaseHelper = new FavoriteDataBaseHelper(context);
        Cursor cursor = mFavoriteDataBaseHelper.getInformation(Constant.FV_VIJA_TABLE_NAME, position);
        if(cursor == null || cursor.getCount() == 0) return false;
        return true;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView primary_tv;
        public TextView secondary_tv;
        public ImageView favorite_iv;
        private Cursor cursor;

        // each data item is just a string in this case
        public ViewHolder(Cursor cursor, View view) {
            super(view);
            this.cursor = cursor;
            // Find fields to populate in inflated template
            primary_tv = (TextView) view.findViewById(R.id.dictionary_listview_primary_tv);
            secondary_tv = (TextView) view.findViewById(R.id.dictionary_listview_secondary_tv);
            favorite_iv = (ImageView) view.findViewById(R.id.dictionary_listview_favorite_iv);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            cursor.moveToPosition(getPosition());
            new HistoryDataBaseHelper(view.getContext())
                    .insertInformations(cursor.getInt(0), cursor.getString(1), new HtmlConverter().getMean(cursor.getString(2)), Constant.HISTORY_VIJA_TYPE);
            Intent intent = new Intent(view.getContext(), ResultActivity.class);
            intent.putExtra("dataHtml", new HtmlConverter().getHtml(cursor.getString(1), cursor.getString(2)));
            intent.putExtra("newWord", cursor.getString(1));
            intent.putExtra("iskanji",false);
            view.getContext().startActivity(intent);
        }
    }
}