package com.example.vetrio.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vetrio.database.HistoryDataBaseHelper;
import com.example.vetrio.utils.Constant;
import com.example.vetrio.converter.HtmlConverter;
import com.example.vetrio.database.FavoriteDataBaseHelper;
import com.example.vetrio.goodict.R;
import com.example.vetrio.goodict.ResultActivity;

public class MainRecyclerKanjiAdapter extends RecyclerView.Adapter<MainRecyclerKanjiAdapter.ViewHolder> {
    private Cursor cursor;
    private Context context;
    private FavoriteDataBaseHelper mFavoriteDataBaseHelper;
    public MainRecyclerKanjiAdapter(Cursor cursor, Context context) {
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_dictionary_kanji, parent, false);
        ViewHolder viewHolder = new ViewHolder(cursor, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.kanji_tv.setText(cursor.getString(1));
        holder.hanviet_tv.setText(cursor.getString(2));
        holder.on_tv.setText("* Onyomi:\t" + cursor.getString(5));
        holder.kun_tv.setText("* Kunyomi:\t" + new HtmlConverter().getKunyomiKanji(cursor.getString(6)));
        holder.nghia_tv.setText("* Nghĩa:\t" + new HtmlConverter().getMeanKanji(cursor.getString(6)));
        if(isFavorite(position)) holder.favorite_iv.setImageResource(R.drawable.ic_star_20dp);
        else holder.favorite_iv.setImageResource(R.drawable.ic_star_outline_20dp);

        holder.favorite_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFavorite(position)) {
                    mFavoriteDataBaseHelper.insertInformations(Constant.FV_KANJI_TABLE_NAME, position);
                    holder.favorite_iv.setImageResource(R.drawable.ic_star_20dp);
                    Toast.makeText(context, "Đã thêm "+ holder.kanji_tv.getText()+ " vào danh sách yêu thích", Toast.LENGTH_SHORT).show();
                }
                else{
                    mFavoriteDataBaseHelper.deleteInfomation(Constant.FV_KANJI_TABLE_NAME, position);
                    holder.favorite_iv.setImageResource(R.drawable.ic_star_outline_20dp);
                    Toast.makeText(context, "Đã hủy " + holder.kanji_tv.getText() + " từ danh sách yêu thích", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
    public boolean isFavorite(int position){
        mFavoriteDataBaseHelper = new FavoriteDataBaseHelper(context);
        Cursor cursor = mFavoriteDataBaseHelper.getInformation(Constant.FV_KANJI_TABLE_NAME, position);
        if(cursor == null || cursor.getCount() == 0) return false;
        return true;
    }
    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView kanji_tv;
        public TextView hanviet_tv;
        public TextView on_tv;
        public TextView kun_tv;
        public TextView nghia_tv;
        public ImageView favorite_iv;
        private Cursor cursor;
        // each data item is just a string in this case
        public ViewHolder(Cursor cursor, View view) {
            super(view);
            this.cursor = cursor;
            // Find fields to populate in inflated template
            kanji_tv = (TextView) view.findViewById(R.id.dictionary_listview_kanji_tv);
            hanviet_tv = (TextView) view.findViewById(R.id.dictionary_listview_hanviet_tv);
            on_tv = (TextView) view.findViewById(R.id.dictionary_listview_on_tv);
            kun_tv = (TextView) view.findViewById(R.id.dictionary_listview_kun_tv);
            nghia_tv = (TextView) view.findViewById(R.id.dictionary_listview_nghia_tv);
            favorite_iv = (ImageView) view.findViewById(R.id.dictionary_listview_favorite_iv);
            view.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            cursor.moveToPosition(getPosition());
            new HistoryDataBaseHelper(view.getContext())
                    .insertInformations(cursor.getInt(0), cursor.getString(1) + "\t\t  "  + cursor.getString(2), new HtmlConverter().getMeanKanji(cursor.getString(6)), Constant.HISTORY_KANJI_TYPE);
            String[] data = new String[7];
            for (int i = 0; i < 7; i++){
                data[i] = cursor.getString(i+1);
            }
            Intent intent = new Intent(view.getContext(), ResultActivity.class);
            intent.putExtra("dataHtml", new HtmlConverter().getHtmlKanji(data));
            intent.putExtra("newWord", data[0]);
            intent.putExtra("iskanji",true);
            intent.putExtra("hanviet", data[1]);
            view.getContext().startActivity(intent);
        }
    }

}