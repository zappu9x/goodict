package com.example.vetrio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vetrio.goodict.R;

/**
 * Created by VetRio on 8/29/2015.
 */
public class MainListViewNavigationAdapter extends ArrayAdapter<String> {

    private Context context;
    private int[] icons;
    private String[] names;

    public MainListViewNavigationAdapter(Context context, String[] names, int[] icons) {
        super(context, 0);
        this.context = context;
        this.names = names;
        this.icons = icons;
    }

    @Override
    public int getCount(){
        if (icons != null)
            return icons.length;
        else return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_listview_navigation, null);
        }

        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        TextView title = (TextView) convertView.findViewById(R.id.title);

        icon.setImageResource(icons[position]);
        title.setText(names[position]);
        return convertView;
    }
}