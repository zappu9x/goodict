package com.example.vetrio.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vetrio.goodict.R;

/**
 * Created by VetRio on 8/29/2015.
 */
public class MainListViewHistoryAdapter extends CursorAdapter {

    private int[] LogoIcons = {R.drawable.bg_round_purple, R.drawable.bg_round_green, R.drawable.bg_round_orange};
    private String[] LogoTexts = {"漢字", "語彙", "語彙"};

    public MainListViewHistoryAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);

    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_lisview_dictionary_history, parent, false);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView logo_tv = (TextView) view.findViewById(R.id.dictionary_listview_logo);
        TextView primary_tv = (TextView) view.findViewById(R.id.dictionary_listview_primary_tv);
        TextView secondary_tv = (TextView) view.findViewById(R.id.dictionary_listview_secondary_tv);
        ImageView favorite_iv = (ImageView) view.findViewById(R.id.dictionary_listview_favorite_iv);

        logo_tv.setBackgroundResource(LogoIcons[cursor.getInt(4)]);
        logo_tv.setText(LogoTexts[cursor.getInt(4)]);
        primary_tv.setText(cursor.getString(1));
        secondary_tv.setText(cursor.getString(2));



    }

}