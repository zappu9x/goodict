package com.example.vetrio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vetrio.customview.fingeritem.FingerSearchItem;
import com.example.vetrio.customview.fingeritem.PercentView;
import com.example.vetrio.goodict.R;

import java.text.DecimalFormat;

public class RecogzineListViewAdapter extends ArrayAdapter<FingerSearchItem> {
    public Context context;
    FingerSearchItem[] fingerSearchItems;

    public RecogzineListViewAdapter(Context context, FingerSearchItem[] fingerSearchItems) {
        super(context, 0, fingerSearchItems);
        this.context = context;
        this.fingerSearchItems = fingerSearchItems;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_listview_kanji_recog, null);
        }

        TextView kanji_word_tv = (TextView) convertView.findViewById(R.id.kanji_word_tv);
        kanji_word_tv.setText(fingerSearchItems[position].getKanji());

        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        TextView scoreTextView = (TextView) convertView.findViewById(R.id.score_tv);
        scoreTextView.setText("Score: \n" + decimalFormat.format(fingerSearchItems[position].getScore()) + "%");

        TextView hanvietTextView = (TextView) convertView.findViewById(R.id.kanji_hanviet_tv);
        if (fingerSearchItems[position].getData() != null)
        {
            String[] data = fingerSearchItems[position].getData();
            hanvietTextView.setText(data[1]);
        }

        PercentView percentView = (PercentView) convertView.findViewById(R.id.percentview);
        percentView.setPercentage(fingerSearchItems[position].getScore());

        return convertView;
    }
}